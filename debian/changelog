evolver (2.70+ds-8) unstable; urgency=medium

  * debianization:
    - d/watch:
      - URL target, update;
    - d/control:
      - Homepage, update;
      - Standards-Version, bump to version 4.5.1 (no change);
    - d/copyright:
      - copyright year tuples, update;
    - d/upstream/metadata:
      - URL, secure.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 21 Feb 2021 15:06:04 +0000

evolver (2.70+ds-7) unstable; urgency=medium

  * Debianization:
    - d/t/control:
      - Architecture field for long double precision tests, refine.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 19 Nov 2020 13:57:35 +0000

evolver (2.70+ds-6) unstable; urgency=medium

  * Debianization:
    - d/t/control:
      - Architecture field for quad precision tests, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 18 Nov 2020 20:50:26 +0000

evolver (2.70+ds-5) unstable; urgency=medium

  * FTBFS fix version, gcc-10 venue, fix faulty patch (Closes:  #957186).
  * Debianization:
    - d/templates/*:
      - d/t/tcontrol.in:
        - debhelper, bump tu 13 (no change);
        - Standards-Version, bumpt to version 4.5.0 (no change);
        - Rules-Requires-Root, introduce and set to no;
        - d/t/evolver-FLAVOUR.lintian-overrides.in, empty;
    - d/patches/*:
      - d/p/upstream-source-display-short_description.patch, FTBFS fix;
      - d/p/upstream-source-contrib-EVOLVERPATH.patch, minor fix;
      - d/p/debianization-*.patch:
        - Forwarded field, add and specify as 'not-needed';
    - d/copyright:
      - copyright year tupls, update;
    - d/changelog, quote malformed email addresses as dtected by lintian;
    - d/evolver-doc.lintian-overrides, discard;
    - d/upstream/metadata, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 14 Nov 2020 20:39:09 +0000

evolver (2.70+ds-4) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Change priority extra to priority optional.
  * Use secure copyright file specification URI.

  [ Jerome Benoit ]
  * Debianization:
    - debian/copyright:
      - Copyright year-tuple, update;
    - debian/{control,templates/control.in}:
      - debhelper, bump to 11;
      - Standards-Version, bump to 4.3.0 (no change);
      - Homepage field, secure;
      - Vcs-* field, migration to salsa;
    - debian/rules:
      - debhelper, bump to 11;
      - pkg-info.mk, include;
      - get-orig-source target, discard;
    - debian/tests/control, introduce;
    - debian/watch:
      - option, compression, add;
    - debian/source/*:
      - lintian-overrides, refresh;
    - debian/{evolver-doc.doc-base,adhoc/Makfile,man/evolver.h2m,
        patches/debianization-build.patch}, debhelper, bump to 11.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 31 Dec 2018 14:06:09 +0000

evolver (2.70+ds-3) unstable; urgency=medium

  * FTBFS fix release (#Closes: 849945), (temporarily) neutralize ld build
    for ppc64el.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 30 Jan 2017 18:31:49 +0000

evolver (2.70+ds-2) unstable; urgency=medium

  * FTBFS fixes release:
    - disable PROFILING on i386 architectures (Closes: #832538);
    - isolate architectures without sysinfo function (Closes: #832540);
    - manage quadruple floating point format support (Closes: #832638).
  * Debianization:
    - debian/patches/:
      - d/p/upstream-source-C2help2man.patch, slightly revisit.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 28 Jul 2016 00:43:59 +0000

evolver (2.70+ds-1) unstable; urgency=medium

  * New maintainer (Closes: #829082).
  * New upstream version (Closes: #745700).
  * Debianization:
    - debian/compat:
      - bump to level 9;
    - debian/copyright:
      - bump format to DEP-5;
      - refresh;
    - debian/control:
      - Standards Version, bump to 3.9.8;
      - Vcs-* headers, secure;
    - debian/watch, revisit;
    - debian/repack, introduce;
    - debian/rules:
      - bump to a fully oriented debhelper script;
      - default target, introduce;
      - get-orig-source target, introduce;
      - build-arch/build-indep scheme, introduce;
      - dpkg-buildflags, set hardening=+all;
    - debian/patches/:
      - bump format to DEP-3;
      - d/p/manpage.patch, revisit
          and rename d/p/upstream-source-manpage.pacth;
      - d/p/debian-opengl.patch, improve
          and rename d/p/debianization-build.patch;
      - d/p/install-doc.patch, revisit
          and rename d/p/debianization-documentation.patch;
      - d/p/example-path.patch, discard in favour of
          d/p/upstream-source-contrib-EVOLVERPATH.patch;
      - d/p/upstream-*.patch, introduce (and submit);
      - d/p/debianization-*.patch, introduce;
    - debian/adhoc/Makefile, introduce;
    - debian/man/evolver.h2m, introduce;
    - refresh.
  * Upstream fixes and enhancements has been submitted to the
    upstream maintainer.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 17 Jul 2016 18:21:13 +0000

evolver (2.30c.dfsg-3) unstable; urgency=low

  * Removed manual230.pdf (closes: #651679).
  * Version 2.30 stopped using gets() so evolver doesn't link with readline,
    removed it from Build-Depends and linker line (closes: #646139).
    Thanks: Daniel T Chen.
  * Added build-arch and build-indep targets to rules.
  * Bumped Standards-Version.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Wed, 14 Dec 2011 17:50:20 -0500

evolver (2.30c-2) unstable; urgency=low

  * No longer has ev225g.tar.gz or any other tarball (closes: #392377).
  * Removed libreadline5-dev from Build-Depends (closes: #553753).
  * Changed to source format 3.0 (quilt).
  * Bumped debhelper compat to 5, and Standards-Version.
  * Corrected evolver-doc doc-base section.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Fri, 25 Jun 2010 07:54:20 -0400

evolver (2.30c-1) unstable; urgency=low

  * New upstream release (closes: #466798).

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Tue, 26 Feb 2008 00:20:50 -0500

evolver (2.26c-1) unstable; urgency=low

  * New upstream version (required by change to binary PDF docs).
  * New PDF documentation is not corrupted (closes: #355081).
  * Changed PDF manual link in doc/install.htm to point to both installed copy
    and online link (closes: #355082).
  * Changed build dependency to freeglut3-dev (closes: #394489).
  * Updated standards version and moved dh version to debian/compat.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Fri,  7 Sep 2007 18:38:58 -0400

evolver (2.26-1) unstable; urgency=low

  * New upstream release.
  * Incorporate Roger Leigh's work with Karl Chen's patch (closes: #317404).
  * Discarded my evolver man page in favor of new upstream.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Mon, 31 Oct 2005 13:32:23 -0500

evolver (2.23-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * src/include.h: Don't prototype calloc() and friends when building
    with an ANSI C compiler (Closes: 317404).  Based on a patch by
    Karl Chen.
  * debian/control: Build-Depend on libreadline5-dev, rather than
    the old libreadline4-dev.

 -- Roger Leigh <rleigh@debian.org>  Fri, 22 Jul 2005 21:16:12 +0100

evolver (2.23-1) unstable; urgency=low

  * New upstream release.
  * Changed Build-Depends from xlibs-dev to libx11-dev (closes: #255962).
  * Removed "prototypes" including calloc from src/include.h (closes: 258657).

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Wed, 11 Aug 2004 12:06:07 -0400

evolver (2.20-1) unstable; urgency=low

  * New upstream (closes: #206787).
  * Forward-ported patch to include evolver-doc examples dir in default path.
  * Changed graphics from "crummy X" to GLUT.
  * Added readline dependency.
  * Tweaked evolver manpage.
  * Updated Standards-Version to 3.6.1.0.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Tue, 26 Aug 2003 10:53:22 -0400

evolver (2.14-5) unstable; urgency=low

  * Changed Build-Depends from xlib6g-dev to xlibs-dev (closes: #170172).
  * Updated Standards-Version to 3.5.8.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Sun, 24 Nov 2002 11:38:11 -0500

evolver (2.14-4) unstable; urgency=low

  * Added symlink from /usr/share/doc/evolver-doc/evolver.htm to default.htm
    since the former is missing and lots of files link the #doc top anchor
    which is only present in the latter (closes: #105395).
  * Bumped Standards-Version since the old one doesn't make sense with
    DH_COMPAT=2.
  * Added a manpage for evolver.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Thu, 20 Sep 2001 15:25:41 -0400

evolver (2.14-3) unstable; urgency=low

  * Added xlib6g-dev to Bulid-Depends, not xlibs-dev so it will still build on
    potato. (closes: #100806)

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Thu, 14 Jun 2001 08:03:45 -0400

evolver (2.14-2) unstable; urgency=low

  * Contacted upstream author for a more thorough and acceptable copyright
    statement.

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Tue, 14 Nov 2000 17:20:58 -0500

evolver (2.14-1) unstable; urgency=low

  * Initial Release (Closes: #75003).

 -- "Adam C. Powell, IV" <hazelsct@debian.org>  Tue,  7 Nov 2000 16:57:55 -0500
