Source: evolver
Section: math
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13), help2man,
 libquadmath0 [amd64 i386 kfreebsd-amd64 kfreebsd-i386 hurd-i386 x32],
 libreadline-dev,
 freeglut3-dev | libglut-dev
Build-Depends-Indep:
 rdfind, symlinks
Standards-Version: 4.5.1
Homepage: https://facstaff.susqu.edu/brakke/evolver/evolver.html
Vcs-Git: https://salsa.debian.org/science-team/evolver.git
Vcs-Browser: https://salsa.debian.org/science-team/evolver

Package: evolver
Architecture: all
Multi-Arch: foreign
Depends: evolver-nox | evolver-ogl , ${misc:Depends}
Description: Surface Evolver
 The Surface Evolver is an interactive program for the modelling of liquid
 surfaces shaped by various forces and constraints.
 .
 This dummy package provides the standard installation.

Package: evolver-nox
Provides: evolver
Breaks: evolver (<< 2.70)
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: geomview
Suggests: evolver-doc
Description: Surface Evolver - with no X support
 The Surface Evolver is an interactive program for the modelling of liquid
 surfaces shaped by various forces and constraints.
 .
 This package provides evolver variants built with different floating point
 formats (double, long double, quadruple) but with no X support.

Package: evolver-ogl
Provides: evolver
Breaks: evolver (<< 2.70)
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: geomview
Suggests: evolver-doc
Description: Surface Evolver - with OpenGL display
 The Surface Evolver is an interactive program for the modelling of liquid
 surfaces shaped by various forces and constraints.
 .
 This package provides evolver variants built with different floating point
 formats (double, long double, quadruple) and with an OpenGL/GLUT display.

Package: evolver-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Suggests: pdf-viewer, www-browser
Enhances: evolver-nox, evolver-ogl
Description: Surface Evolver - doc
 The Surface Evolver is an interactive program for the modelling of liquid
 surfaces shaped by various forces and constraints.
 .
 This package provides the documentation and the on-line help content for the
 evolver front-end program, as well as examples.
