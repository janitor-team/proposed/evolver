help2man include file for evolver man page

[DESCRIPTION]
The
.B Surface Evolver
is an interactive program for the modelling of liquid surfaces shaped by various forces and constraints.
Basically the
.B Surface Evolver
minimizes the energy of triangulated surfaces according to designated energies and constraints.

This man page mainly documents command line options and environment variables.

[ENVIRONMENT]
.IP EVOLVERPATH
Colon-separated list of paths automatically searched for datafiles, included files,
or on-line help documentation.
By default, the paths
.IR /usr/share/doc/evolver/html
and
.IR /usr/share/doc/evolver/examples
are appended (in that order);
this default behaviour can be cancelled by appending
at least three consecutive colons (":::") at its end.


[FILES]
.I ~/.evolver_history
.RS
Per user command history file.
.RE

.I /usr/share/doc/evolver/manual.pdf
.RS
The (complete)
.B Surface Evolver Manual
in PDF format.
.RE

.I /usr/share/doc/evolver/EvolverDoc.html
.RS
The HTML version of the
.B Surface Evolver Manual
that contains what the PDF manual has,
modulo some advanced material.
.RE

.I /usr/share/doc/evolver/examples/
.RS
This folder contains sample material,
mainly datafiles and command-files,
for the
.B Surface Evolver.
.RE


[AVAILABILITY]
The latest version of the
.B Surface Evolver,
up-to-date documentation, and more are available on-line from:

    http://www.susqu.edu/brakke/evolver


[AUTHOR]
Ken Brakke <brakke@susqu.edu>
has written and currently maintained the core part of the
.B Surface Evolver.
This man page was written for Debian by
Jerome Benoit <calculus@rezozer.net>.


[SEE ALSO]
The complete manual for the
.B Surface Evolver
is available in PDF format
in the documentation folder as
.IR /usr/share/doc/evolver/manual.pdf .

.BR geomview (1),
.BR polymerge (1),
.BR anytooff (1),
.BR oogl (5),
.BR readline (3)
