Description: source: contrib: EVOLVERPATH
 Attempt to implement a function, getenv_EVOLVERPATH, for a better
 management of the environment variable EVOLVERPATH: it is got only
 once; a default path list defined at building time can be appended
 or not on fly at running time. See the Purpose field and the NOTE
 in src/Makefile for more inforamtion. Meant to be suggested to the
 upstream maintainer.
Origin: debian
Forwarded: by email
Author: Jerome Benoit <calculus@rezozer.net>
Last-Update: 2020-11-14

--- a/src/utility.c
+++ b/src/utility.c
@@ -13,6 +13,74 @@
 #include "include.h"
 
 /**************************************************************
+*
+*  Function: getenv_EVOLVERPATH()
+*
+*  Purpose: get the environment variable EVOLVERPATH for once and
+*        append the builtin path ENVEVOLVERPATHBUILTIN if it is
+*        defined and if EVOLVERPATH does not terminate with the
+*        magic triplet ENVPATHCHAR+ENVPATHCHAR+ENVPATHCHA. Checks
+*        and alterations might be implemented in future.
+*/
+
+/* const */ char * getenv_EVOLVERPATH(void)
+{	static int uninitialized = 1;
+	static char *env_EVOLVERPATH = NULL;
+	if (uninitialized)
+	{ const char *environment_EVOLVERPATH = getenv("EVOLVERPATH");
+		if (environment_EVOLVERPATH != NULL)
+		{	const size_t lengthof_EVOLVERPATH = strlen(environment_EVOLVERPATH);
+			size_t sizeof_EVOLVERPATH =
+				lengthof_EVOLVERPATH
+#ifdef ENVEVOLVERPATHBUILTIN
+				+ sizeof( ENVPATHCHAR ENVEVOLVERPATHBUILTIN )
+#else
+				+ 1
+#endif
+				;
+#ifdef ENVEVOLVERPATHBUILTIN
+			int builtinpath_flag = 1;
+#endif
+			env_EVOLVERPATH = (char *) malloc(sizeof_EVOLVERPATH);
+			strcpy(env_EVOLVERPATH,environment_EVOLVERPATH);
+			if (3 <= lengthof_EVOLVERPATH)
+			{	char *magictriplet = env_EVOLVERPATH + lengthof_EVOLVERPATH - 3;
+				if (!(strcmp(magictriplet, ENVPATHCHAR ENVPATHCHAR ENVPATHCHAR )))
+				{	*magictriplet = '\0';
+#ifdef ENVEVOLVERPATHBUILTIN
+					builtinpath_flag = 0;
+#endif
+				}
+			}
+			if (*env_EVOLVERPATH != '\0')
+			{	const char ENVPATHCHAR_str[] = ENVPATHCHAR;
+				const char char_ENVPATHCHAR = ENVPATHCHAR_str[0];
+				char *dum = env_EVOLVERPATH + strlen(env_EVOLVERPATH) - 1;
+				while ((*dum == char_ENVPATHCHAR) && (env_EVOLVERPATH <= dum)) { *dum = '\0'; --dum; }
+			}
+#ifdef ENVEVOLVERPATHBUILTIN
+			if (builtinpath_flag)
+			{	if (*env_EVOLVERPATH == '\0') strcpy(env_EVOLVERPATH, ENVEVOLVERPATHBUILTIN );
+				else strcat(env_EVOLVERPATH, ENVPATHCHAR ENVEVOLVERPATHBUILTIN );
+			}
+			else
+#endif
+			{	if (*env_EVOLVERPATH == '\0')
+				{ free(env_EVOLVERPATH); env_EVOLVERPATH = NULL; }
+			}
+		}
+#ifdef ENVEVOLVERPATHBUILTIN
+		else
+		{
+			env_EVOLVERPATH = ENVEVOLVERPATHBUILTIN ;
+		}
+#endif
+		uninitialized = 0;
+	}
+	return env_EVOLVERPATH;
+}
+
+/**************************************************************
 *  
 *  Function: catcher()
 *
@@ -3131,7 +3199,7 @@
   name = taskpath;
 #endif
 
-  env = getenv("EVOLVERPATH");
+  env = getenv_EVOLVERPATH();
 
 #if defined(WIN32) && !defined(__BORLANDC__)
   /* Using wildcards! */
@@ -3173,7 +3241,7 @@
   /* try .fe extension */
   if ( fd == NULL)
   {
-    env = getenv("EVOLVERPATH");
+    env = getenv_EVOLVERPATH();
     strncpy(path,name,sizeof(path));
     strcat(path,".fe");
     for ( ;; )
@@ -3244,7 +3312,7 @@
   /* try .fe extension */
   if ( fd == NULL)
   {
-    env = getenv("EVOLVERPATH");
+    env = getenv_EVOLVERPATH();
     strncpy(path,name,sizeof(path));
     strcat(path,".fe");
     for ( ;; )
@@ -3292,7 +3360,7 @@
   /* try .fe extension */
   if ( fd == NULL)
   {
-    env = getenv("EVOLVERPATH");
+    env = getenv_EVOLVERPATH();
     strncpy(path,name,sizeof(path));
     strcat(path,".fe");
     while ( (fd = fopen(path,"r")) == NULL)
--- a/src/proto.h
+++ b/src/proto.h
@@ -845,6 +845,7 @@
 void constr_edge_force_q (edge_id);
 void restore_vertex (vertex_id,struct oldcoord *,int);
 REAL normal_change_check(void);
+char * getenv_EVOLVERPATH(void);
 FILE *path_open (char *,int);
 void fil_finish(void);
 void fil_facet (struct tsort *);
--- a/src/userfunc.c
+++ b/src/userfunc.c
@@ -200,7 +200,7 @@
      kb_error(2202,"Too many dynamic load libraries.\n",DATAFILE_ERROR);
 
 
-  env = getenv("EVOLVERPATH");
+  env = getenv_EVOLVERPATH();
 
   /* try current directory first */
   strcpy(path,"./");
--- a/src/Makefile
+++ b/src/Makefile
@@ -28,6 +28,13 @@
 # and may be incompatible with readline libraries on some systems (Mac,
 # for instance).
 
+# NOTE: A default builtin path ENVEVOLVERPATHBUILTIN can be appended to the
+# environment variable EVOLVERPATH at running time by adding to CFLAGS
+# -DENVEVOLVERPATHBUILTIN=\"/path1/to/evolver/stuff:/path0/to/evolver/stuff\".
+# Whenever EVOLVERPATH terminates with 3 path separators (e.g., ":::" on Unix
+# like systems), ENVEVOLVERPATHBUILTIN is not appended (and the magic triplet
+# is removed).
+
 #-----------------------------------------------------------------------------
 
 # Pick your compiler, and add any options you want, such as optimization.
